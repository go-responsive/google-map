import Vue from 'vue'
import App from './App.vue'
// import bootstrap for our grid system and other components
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
// import google maps library
import * as VueGoogleMaps from "vue2-google-maps";
// import text highlighter for search terms
import TextHighlight from 'vue-text-highlight';

// Set up the Google Maps API key
Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyBHNcH18zqWti9nVUETeRtzFERQVIa4GEo"
  }
});

// Set up highlighting component
Vue.component('text-highlight', TextHighlight);

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')

